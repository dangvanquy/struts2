package com.quycntt.dao;

import com.quycntt.entity.Food;

import java.util.List;

public interface FoodDao {
    List<Food> getListFood();
    List<Food> getListFoodNew();
    List<Food> getListFoodDescription();
    Food getFood1(int id);
}