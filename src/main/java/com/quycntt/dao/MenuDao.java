package com.quycntt.dao;

import com.quycntt.entity.Menu;

import java.util.List;

public interface MenuDao {
    List<Menu> getListMenu();
}