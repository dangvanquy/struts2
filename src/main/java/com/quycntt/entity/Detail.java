package com.quycntt.entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Entity(name = "detail")
public class Detail {
    @Id
    private int id;

    @Column(name = "quantity")
    private int quantity;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "foodid")
    Food food;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }
}