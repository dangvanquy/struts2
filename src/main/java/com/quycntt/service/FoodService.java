package com.quycntt.service;

import com.quycntt.dao.FoodDao;
import com.quycntt.daoimp.FoodImp;
import com.quycntt.entity.Food;

import java.util.List;

public class FoodService implements FoodDao {
    private FoodImp foodImp;

    public void setFoodImp(FoodImp foodImp) {
        this.foodImp = foodImp;
    }

    public List<Food> getListFood() {
        return foodImp.getListFood();
    }

    public List<Food> getListFoodNew() {
        return foodImp.getListFoodNew();
    }

    public List<Food> getListFoodDescription() {
        return foodImp.getListFoodDescription();
    }

    public Food getFood1(int id) {
        return foodImp.getFood1(id);
    }
}