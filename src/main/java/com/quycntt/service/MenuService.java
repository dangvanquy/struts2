package com.quycntt.service;

import com.quycntt.dao.MenuDao;
import com.quycntt.daoimp.MenuImp;
import com.quycntt.entity.Menu;

import java.util.List;

public class MenuService implements MenuDao {
    private MenuImp menuImp;

    public void setMenuImp(MenuImp menuImp) {
        this.menuImp = menuImp;
    }
    public List<Menu> getListMenu() {
        return menuImp.getListMenu();
    }
}