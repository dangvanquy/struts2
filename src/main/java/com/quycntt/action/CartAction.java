package com.quycntt.action;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.quycntt.entity.Bill;
import com.quycntt.entity.User;
import com.quycntt.service.BillService;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class CartAction extends ActionSupport implements ServletRequestAware, SessionAware{
    private BillService billService;
    private int id;
    private String name;
    private double price;
    private int quantity;
    private User user;
    private int iduser;
    private String mes;
    private boolean check;
    private HttpServletRequest request;
    private Map<String, Object> session;

    public Map<String, Object> getSession() {
        return session;
    }

    public void setBillService(BillService billService) {
        this.billService = billService;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isCheck() {
        return check;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public User getUser() {
        return user;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String addBill() {
        Bill bill = new Bill();
        Map session = ActionContext.getContext().getSession();
        User user = (User) session.get("users");
        bill.setName(name);
        bill.setPrice(price);
        bill.setQuantity(quantity);
        bill.setUser(user);
        check = billService.insertBill(bill);

        return SUCCESS;
    }

    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}