<%@page import="java.util.List"%>
<%@ page contentType="text/html;charset=UTF-8"  language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>home</title>
    <link rel="stylesheet" href='<c:url value="../../resources/css/bootstrap.css"/> '/>
    <link rel="stylesheet" href='<c:url value ="../../resources/css/freshfood.css"/>'/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
</head>
<body>
<div id="full">

    <!-- menu -->
    <div class="row" id="header">
        <div id="logo" class="col-lg-4 col-md-4 col-sm-12 col-12">
            <img src="resources/images/logo.png">
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-12">
            <div class="row">
                <div id="menu" class="col-lg-9 col-md-9 col-sm-12 col-12">
                    <ul>
                        <c:forEach items="${listMenu}" var="l">
                            <li><a href="<c:out value="${l.getLink()}"/>"><c:out value="${l.getName()}"/></a></li>
                        </c:forEach>


                        <c:choose>
                            <c:when test="${users != null }">

                                <div class="name">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <c:out value="${users.getName() }"/>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <c:forEach items="${users.getUserRoles() }" var="u">
                                                <a class="dropdown-item" href="#"><c:out value="${u.getRoleid().getName() }"/></a>
                                            </c:forEach>
                                            <a class="dropdown-item" href="logout">Logout</a>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <li><a href="loginexe">đăng nhập</a></li>
                                <li><a href="registerexe">đăng kí</a></li>
                            </c:otherwise>
                        </c:choose>

                    </ul>


                </div>

                <div id="icon" class="col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-md-4">
                            <i class="fa fa-search"></i>
                        </div>
                        <div class="col-md-4">
                            <a href="cartexe"><i class="fa fa-shopping-cart"></i></a>
                        </div>
                        <div class="col-md-4">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- slide -->
    <div id="slide" class="row">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="resources/images/slide.png" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="resources/images/slide.png" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="resources/images/slide.png" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <!-- sanpham -->
    <div id="sanpham" class="container">
        <div class="row">
            <c:forEach items="${listFood}" var="listFood1">
                <div class="col-lg-4 col-md-4">
                    <a href="detailexe?id=<c:out value="${listFood1.getId()}"/>"><img src='resources/<c:url value="${listFood1.getImage()}"/> '></a>
                </div>
            </c:forEach>
        </div>
    </div>

    <!-- san pham moi -->
    <div id="thucphammoi">
        <div class="container">
            <div class="row">
                <p class="thucphammoip">THỰC PHẨM MỚI</p>
            </div>
            <div class="row">
                <i class="fa fa-search-plus"></i>
            </div>
            <div id="imagesanphammoi" class="row">
                <c:forEach items="${listFood1}" var="listFoodNew1">
                    <div class="col-md-3">
                        <a href="detailexe?id=<c:out value="${listFoodNew1.getId()}" />">
                        <img src='resources/<c:url value="${listFoodNew1.getImage()}" />'>
                        <div class="thongtin">
                            <hr>
                            <p class="tensanpham"><c:out value="${listFoodNew1.getName()}"/> </p>
                            <p class="gia"><c:out value="${listFoodNew1.getPrice()}" />VNĐ</p>
                            <p class="giohang">Thêm vào giỏ hàng</p>
                        </div>
                        </a>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

    <!-- cam nang -->
    <div id="camnang">
        <div class="container">
            <div class="row">
                <p id="pcamnang">CẨM NANG NẤU ĂN</p>
            </div>
            <div class="row">
                <i></i>
            </div>
            <div id="imgcamnang" class="row">
                <c:forEach items="${listFood2}" var="listFoodDescription1">
                    <div class="col-md-4">
                        <a href="detailexe?id=<c:out value="${listFoodDescription1.getId()}" />">
                        <img src='resources/<c:url value="${listFoodDescription1.getImage()}"/> '>
                        <p class="tencamnang"><c:out value="${listFoodDescription1.getName()}"/></p>
                        </a>
                    </div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>

    <!-- footer -->
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p class="ptext">Trụ sở chính</p>
                    <p class="ptext2">Số 6A8A, Thanh Xuân Bắc, Phường Thanh Xuân Bắc, Quận Thanh Xuân, Thành phố Hà Nội.</p>
                    <p class="dienthoai">Điện thoại: 04.3311.3998</p>
                    <p class="fax">Fax: 04.3311.3996</p>
                </div>
                <div class="col-md-4">
                    <p class="ptext">Chi nhánh Đà Nẵng</p>
                    <p class="ptext2">Số 6A8A, Thanh Xuân Bắc, Phường Thanh Xuân Bắc, Quận Thanh Xuân, Thành phố Hà Nội.</p>
                    <p class="dienthoai">Điện thoại: 04.3311.3998</p>
                    <p class="fax">Fax: 04.3311.3996</p>
                </div>
                <div class="col-md-4">
                    <p class="ptext">Chi nhánh Hồ Chí Minh</p>
                    <p class="ptext2">Số 6A8A, Thanh Xuân Bắc, Phường Thanh Xuân Bắc, Quận Thanh Xuân, Thành phố Hà Nội.</p>
                    <p class="dienthoai">Điện thoại: 04.3311.3998</p>
                    <p class="fax">Fax: 04.3311.3996</p>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</body>
</html>
